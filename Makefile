.PHONY: all start stop yeet

all: start

start:
	docker-compose up -d --force-recreate

stop:
	docker-compose kill
	docker-compose rm -f

yeet: stop
	rm -rf /opt/minecraft/data
